------------1:
CREATE USER rentaluser WITH PASSWORD 'rentalpassword';

GRANT CONNECT ON DATABASE dvd_rental TO rentaluser;


------------2:
GRANT SELECT ON TABLE customer TO rentaluser;

SET ROLE rentaluser;

SELECT * FROM customer;

---Reset role (after point 2)
RESET ROLE;


------------3:
CREATE ROLE rental;

GRANT rental TO rentaluser;


------------4:
GRANT SELECT, INSERT, UPDATE ON TABLE rental TO rental;

SET ROLE rental;

INSERT INTO rental (rental_id, rental_date, inventory_id, customer_id, return_date, staff_id, last_update)
VALUES (16050, '2024-06-20 11:00:00', 1000, 300, '2024-07-01 10:00:00', 2, CURRENT_TIMESTAMP);

UPDATE rental
SET return_date = '2024-07-02 10:00:00', last_update = CURRENT_TIMESTAMP
WHERE rental_id = 16050;

---Reset role (after point 4)
RESET ROLE;


------------5:
REVOKE INSERT ON TABLE rental FROM rental;

SET ROLE rental;

INSERT INTO rental (rental_id, rental_date, inventory_id, customer_id, return_date, staff_id, last_update)
VALUES (16051, '2024-06-26 10:00:00', 1001, 301, '2024-07-02 10:00:00', 2, CURRENT_TIMESTAMP);

---Reset role (after point 5)
RESET ROLE;


------------6:
DO $$
DECLARE
    customer_rec RECORD;
    role_name TEXT;
    rental_policy_name TEXT;
    payment_policy_name TEXT;
BEGIN
    SELECT c.customer_id, c.first_name, c.last_name
    INTO customer_rec
    FROM customer c
    JOIN payment p ON c.customer_id = p.customer_id
    JOIN rental r ON c.customer_id = r.customer_id
    GROUP BY c.customer_id, c.first_name, c.last_name
    HAVING COUNT(p.payment_id) > 0 AND COUNT(r.rental_id) > 0
    LIMIT 1;

    IF NOT FOUND THEN
        RAISE NOTICE 'No customer found with non-empty payment and rental history.';
        RETURN;
    END IF;

    role_name := format('client_%s_%s', customer_rec.first_name, customer_rec.last_name);
    rental_policy_name := format('rental_policy_%s_%s', customer_rec.first_name, customer_rec.last_name);
    payment_policy_name := format('payment_policy_%s_%s', customer_rec.first_name, customer_rec.last_name);

    EXECUTE format('CREATE ROLE %I WITH LOGIN PASSWORD %L', role_name, 'securepassword');

    EXECUTE format('GRANT SELECT ON rental TO %I', role_name);
    EXECUTE format('GRANT SELECT ON payment TO %I', role_name);

    EXECUTE format('
        CREATE POLICY %I
        ON rental
        FOR SELECT
        USING (customer_id = %L)',
        rental_policy_name,
        customer_rec.customer_id
    );

    EXECUTE format('
        CREATE POLICY %I
        ON payment
        FOR SELECT
        USING (customer_id = %L)',
        payment_policy_name,
        customer_rec.customer_id
    );

    EXECUTE 'ALTER TABLE rental ENABLE ROW LEVEL SECURITY';
    EXECUTE 'ALTER TABLE payment ENABLE ROW LEVEL SECURITY';

    EXECUTE 'ALTER TABLE rental FORCE ROW LEVEL SECURITY';
    EXECUTE 'ALTER TABLE payment FORCE ROW LEVEL SECURITY';

    RAISE NOTICE 'Role "%" created for customer % %.', role_name, customer_rec.first_name, customer_rec.last_name;
END $$;

--The resulting role contains quotes due to formatting issues. Hope it's not a big problem :)
SET ROLE "client_PATRICIA_JOHNSON";

SELECT * FROM rental;

SELECT * FROM payment;

-- Reset role
RESET ROLE;


--If there is a need to drop the role:

--REVOKE ALL PRIVILEGES ON TABLE rental FROM "client_PATRICIA_JOHNSON";
--REVOKE ALL PRIVILEGES ON TABLE payment FROM "client_PATRICIA_JOHNSON";
--DROP POLICY "rental_policy_PATRICIA_JOHNSON" ON rental;
--DROP POLICY "payment_policy_PATRICIA_JOHNSON" ON payment;
--DROP ROLE "client_PATRICIA_JOHNSON";
